#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG2

set multiplot layout 1, 2

set xlabel "Angle"
set ylabel "Probability"

yMin =    0.0

set title  "Angular distribution functions, from oxygen, ".ARG3
plot[0:180][yMin:] ARG1 u 2:3 w l title "O-O-O" \
                 , ARG1 u 2:4 w l title "O-O-M" \
                 , ARG1 u 2:5 w l title "M-O-M"

set title  "Angular distribution functions, from metal, ".ARG3
unset ylabel
plot[0:180][yMin:] ARG1 u 2:6 w l title "O-M-O" \
                 , ARG1 u 2:7 w l title "O-M-M" \
                 , ARG1 u 2:8 w l title "M-M-M"


unset multiplot
unset output
