#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG2

set multiplot layout 1, 2

set pm3d map
set palette

stats ARG1 nooutput
max_col = STATS_columns

set title  ("Oxygen percentage, ".ARG3)
set xlabel "Time (ps)"
set ylabel "Position (Ångström)"
set cbrange [0:1]
splot[0:][-25:25][0:5] ARG1 u ($1/ARG5):2:(0) title ""                       \
                     , ARG1 u ($1/ARG5):2:( $3                               \
                                         / (sum[col=3:max_col] column(col))) \
                                              title ""

unset ylabel
set cbrange [0:1]
set title  ("Metal percentage, ".ARG3)
splot[0:][-25:25][0:8] ARG1 u ($1/ARG5):2:(0) title ""                       \
                     , ARG1 u ($1/ARG5):2:((sum[col=4:max_col] column(col))  \
                                         / (sum[col=3:max_col] column(col))) \
                                              title ""
unset multiplot
unset output
