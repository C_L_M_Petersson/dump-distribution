#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG3

set multiplot layout 1, 2

set pm3d map
set palette

stats ARG1 nooutput
maxMetalCol = STATS_columns

stats ARG2 nooutput
nPeaks      = STATS_columns

set title  ("Oxygen ".ARG4)
set xlabel "Time (ps)"
set ylabel "Position (Ångström)"
set cbrange [0:]
splot[0:][-25:25][0:] ARG1 u ($1/ARG6):2          :($3/ARG5) title "" \
   , for [i=2:nPeaks] ARG2 u ($1/ARG6):(column(i)):(0.0)              \
                                     w l lw 3 lc rgb 'white' title ""

unset ylabel
set title  ("Metal ".ARG4)
set cbrange [0:]
splot[0:][-25:25][0:] ARG1 u                                          \
    ($1/ARG6):2:((sum [col=4:maxMetalCol] column(col))/ARG5) title "" \
   , for [i=2:nPeaks] ARG2 u ($1/ARG6):(column(i)):(0.0)              \
                                     w l lw 3 lc rgb 'black' title ""

unset multiplot
unset output
