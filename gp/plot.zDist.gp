#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG2

set multiplot layout 1, 2

set pm3d map
set palette

stats ARG1 nooutput
maxMetalCol = STATS_columns

set title  ("Oxygen ".ARG3)
set xlabel "Time (ps)"
set ylabel "Position (Ångström)"
set cbrange [0:]
splot[0:][-25:25][0:] ARG1 u \
    ($1/ARG5):2:($3/ARG4)                                 title ""

unset ylabel
set cbrange [0:]
set title  ("Metal ".ARG3)
splot[0:][-25:25][0:] ARG1 u \
    ($1/ARG5):2:(sum[col=4:maxMetalCol] column(col)/ARG4) title ""

unset multiplot
unset output
