#!/bin/gnuplot

set term png size 3840,2160 font "Times-New-Roman,24"
set output ARG2

set multiplot layout 2, 4

set pm3d map
set palette

set key left
set ylabel "Angle (deg)"

yMin =    0.0
yMax =    0.5

set cbrange [0:]

unset xlabel
set title "O-OO bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:4

unset ylabel
set title "O-OM bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:5

unset ylabel
set title "M-OO bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:6

set title "M-OM bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:7

set ylabel "Angle (deg)"
set xlabel "Distance (Å)"
set title "0-M0 bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:8

set title "0-MM bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:9

set title "M-MO bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:10

set title "M-MM bond density, ".ARG3
splot[0:][0:180][0:] ARG1 u 2:3:11
