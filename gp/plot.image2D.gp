#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"

set pm3d map
set palette

ndirs=system("egrep '^(\-|)[0-9]' " . ARG1 . " | awk '{print $1}'" \
                                           . " | awk '!x[$0]++'  " \
                                           . " | wc -l")

do for [dirIndex=1:ndirs] {
    fst=system(" pcregrep -n -M '^\n\n' " . ARG1                           \
                                          . " | grep : "                   \
                                          . " | cut -d: -f1 "              \
                                          . " | sed '1s;^;0\\n;' "         \
                                          . " | sed '" . dirIndex . "q;d'" )
    snd=system(" pcregrep -n -M '^\n\n' " . ARG1                           \
                                          . " | grep : "                   \
                                          . " | cut -d: -f1 "              \
                                          . " | sed '" . dirIndex . "q;d'" )
    dir =system("sed '" . (fst+2) . "q;d' ". ARG1 . " | awk '{print $1}'" )
    xDir=system("sed '" . (fst+2) . "q;d' ". ARG1 . " | awk '{print $2}'" )
    yDir=system("sed '" . (fst+2) . "q;d' ". ARG1 . " | awk '{print $3}'" )

    set output (ARG2.".".dir.".png")
    set multiplot layout 1,2

    set title  ("Oxygen in the ".dir."-direction".ARG3)
    set xlabel xDir." (Ångström)"
    set ylabel yDir." (Ångström)"
    set cbrange [0:]
    splot[-50:50][-50:50]                                     \
        ("<awk 'NR>=" . fst . " && NR<=" . snd . "' " . ARG1) \
        u 5:6:7       title ""

    set title  ("Metal in the ".dir."-direction".ARG3)
    unset ylabel
    set cbrange [0:]
    splot[-50:50][-50:50]                                     \
        ("<awk 'NR>=" . fst . " && NR<=" . snd . "' " . ARG1) \
        u 5:6:($8+$9) title ""

    unset multiplot
    unset output
}
