#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG2

set key left
set xlabel "Distance"
set ylabel "Probability"

rgbOO = '#0000FF'
rgbOM = '#00FF00'
rgbMM = '#FF0000'

yMin =    0.0
yMax =    0.5

set title  "Radial distribution functions, ".ARG3
plot[0:][yMin:yMax] ARG1 u 2:3 w l lw 2 lt rgb rgbOO title "Oxygen-Oxygen" \
                  , ARG1 u 2:4 w l lw 2 lt rgb rgbOM title "Oxygen-Metal"  \
                  , ARG1 u 2:5 w l lw 2 lt rgb rgbMM title "Metal-Metal"
