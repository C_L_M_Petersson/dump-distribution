#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG2

set title  (ARG3)
set xlabel "Time (ps)"
set ylabel "Oxygen atoms in layer (normalised for lattice streaching)"

plot[][0:] ARG1 u ($1/ARG5):(($4+$12)/2/ARG4) w l title "Below layer 1" \
         , ARG1 u ($1/ARG5):(($5+$11)/2/ARG4) w l title "Below layer 2" \
         , ARG1 u ($1/ARG5):(($6+$10)/2/ARG4) w l title "Below layer 3" \
         , ARG1 u ($1/ARG5):(($7+ $9)/2/ARG4) w l title "Below layer 4"
