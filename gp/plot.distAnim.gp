#!/bin/gnuplot

#set term png size 1920,1080 font "Times-New-Roman,24"
set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG3

#set output (ARG)

print( "1:  ".ARG1  )
print( "2:  ".ARG2  )
print( "3:  ".ARG3  )
print( "4:  ".ARG4  )
print( "5:  ".ARG5  )
print( "6:  ".ARG6  )
print( "7:  ".ARG7  )
print( "8:  ".ARG8  )
print( "9:  ".ARG9  )

INFILEZLIST=ARG1
INFILEZPEAKS=ARG2

OUTFILE=ARG3

DUMPNR=ARG9
TIMESTEP=ARG8*(ARG9-1)
TIME=ARG8/ARG6

print( DUMPNR   )
print( TIMESTEP )
print( TIME     )
print("-----")

system("egrep \"^".TIMESTEP." \" ".INFILEZPEAKS)
#tail -n+$(((".ARG7."+1)*".DUMPNR."+1)) ".ARG1." | head -n".ARG7)
system("tail -n+$(( (".ARG7."+1)*".DUMPNR."+1)) ".ARG1." | head -n".ARG7." | sed \"s/$/$(egrep \\\"^".TIMESTEP." \\\" ".INFILEZPEAKS")/\"" )

#plot ("< tail -n+$((".ARG7."+2)) ".ARG1." | head -n".ARG7) u 2:3
#system("tail -n+$((".ARG7."+2)) ".ARG1." | head -n".ARG7." | sed \"s/$/""/\"" )

#system("tail -n+$((".ARG7"."+2)) ARG1 | head -n$ZLEN | sed \"s/$/ more text/\"")
#system(tail -n+$((ARG7+2)) ARG1 | head -n$ZLEN | sed "s/$/ more text/")

#plot
