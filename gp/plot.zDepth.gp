#!/bin/gnuplot

set term png size 1920,1080 font "Times-New-Roman,24"
set output ARG2

#set multiplot layout 1, 2
#
#set pm3d map
#set palette

set xlabel "Time (ps)"
set ylabel "Thickness (Ångström)"

plot[][0:] ARG1 u 1:($3-$2)       w l title "     Total Metal" \
         , ARG1 u 1:($5-$4)       w l title "Unoxidated Metal" \
         , ARG1 u 1:($3-$2-$5+$4) w l title "  Oxidated Metal"
