# {{{ GENERAL SETTINGS

all:
	-rm $(EXECUTABLE)
	make $(EXECUTABLE)

SRCDIR=src

BUILDDIR=localbuild
HIDIR=$(BUILDDIR)/hi/
ODIR=$(BUILDDIR)/o/
EXECUTABLE=run.x

MAIN=$(SRCDIR)/Main.hs

# }}}

# HASKELL SETTINGS {{{

GHC=ghc --make
GHCDIRS=-odir $(ODIR) -hidir $(HIDIR) -i$(SRCDIR)
GHCOPTS=-dynamic -O2 -XGADTs $(patsubst %, -package %, $(GHCPKGS))
GHCPKGS=base              \
        composition       \
        composition-extra \
        containers        \
        extra             \
        lens              \
        monad-loops       \
        monadlist         \
        mtl               \
        safe              \
        split             \
        Unique            \
        utility-ht

# }}}

# CPP SETTINGS {{{

CPPINCLUDE=
CPPLIBS=-L/usr/lib/openmpi

# }}}

# COMPILATION {{{

$(EXECUTABLE): $(MAIN) $(ODIR) $(HIDIR)
	$(GHC) -o $@ $(GHCDIRS) $(GHCOPTS) $(CPPINCLUDE) $(CPPLIBS) $<

# }}}

# ADMINISTRATIVE{{{

%/:
	mkdir -p $@

clean:
	rm -r $(BUILDDIR)

# }}}
