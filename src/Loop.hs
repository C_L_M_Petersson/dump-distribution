{-# LANGUAGE LambdaCase #-}
module Loop where

import           Control.Monad

import           Data.Maybe

import           Loop.Pretreatment

import           MolDyn
import           MolDyn.Dump.Utils
import           MolDyn.Treatment.Angular
import           MolDyn.Treatment.Charge
import           MolDyn.Treatment.Depth
import           MolDyn.Treatment.Distribution
import           MolDyn.Treatment.Layers
import           MolDyn.Treatment.Peaks
import           MolDyn.Treatment.Radial

import           Output


data Task = CFG
          | Print
          | Dist
          | Peaks
          | OxygenLayers
          | Depth
          | Radial
          | Angular
          | AngularRadial
          | Image2DFromDir
          | ChargeWithin
    deriving(Read)


loop :: MolDyn ()
loop = applyAllTreatments>>getReadOption "task">>= \case
    CFG            -> nullHeader              >>=putStrLn' >> cfgLoop
    Print          -> nullHeader              >>=putStrLn' >> printLoop
    Dist           -> nullHeader              >>=putStrLn' >> distributionLoop
    Peaks          -> nullHeader              >>=putStrLn' >> peakPositionLoop
    OxygenLayers   -> nullHeader              >>=putStrLn' >> oxygenByLayerLoop
    Depth          -> nullHeader              >>=putStrLn' >> depthLoop
    Radial         -> radialHeader True       >>=putStrLn' >> radialLoop
    Angular        -> nullHeader              >>=putStrLn' >> angularLoop
    AngularRadial  -> nullHeader              >>=putStrLn' >> angularRadialLoop
    Image2DFromDir -> nullHeader              >>=putStrLn' >> image2DFromDirLoop
    ChargeWithin   -> chargeWithinHeader False>>=putStrLn' >> chargeWithinLoop
    where nullHeader = return "" :: MolDyn String


cfgLoop :: MolDyn ()
cfgLoop = getCfg>>=print'

printLoop :: MolDyn ()
printLoop = mapDumpM_ (getDump>>=putStr' . show)

distributionLoop :: MolDyn ()
distributionLoop = mapDumpM_ $ dumpAtomKindDists>>=showXDists>>=putStrLn'

peakPositionLoop :: MolDyn ()
peakPositionLoop = getPeaks>>=
    mapDumpM_ . (evolvePeaks>=>showMaxima>=>putStrLn')

oxygenByLayerLoop :: MolDyn ()
oxygenByLayerLoop = getPeaks>>=
    mapDumpM_ . (evolvePeaks>=>oxygenLayers>=>showAtomsPerCells>=>putStrLn')

depthLoop :: MolDyn ()
depthLoop = mapDumpM_ $ (:)<$>dumpTime<*>(catMaybes<$>giveDepths)
                >>=putStrLn' . unwords . map show

radialLoop :: MolDyn ()
radialLoop = mapDumpM_
    (getDump>>= \d -> radial True d d>>=showXDists>>=putStrLn')

angularLoop :: MolDyn ()
angularLoop = mapDumpM_ (cosAngDist>>=showAngDists . map head>>=putStrLn')

angularRadialLoop :: MolDyn ()
angularRadialLoop = mapDumpM_ (cosAngRadDist>>=showXAngDists>>=putStrLn')

image2DFromDirLoop :: MolDyn ()
image2DFromDirLoop = mapDumpM_
    (image2D>>=mapM_ (uncurry showImage2DDists>=>putStrLn'))

chargeWithinLoop :: MolDyn ()
chargeWithinLoop = mapDumpM_ (chargeWithin False>>=showQQ'Dists>>=putStrLn')
