module Configure where

import           Data.Char
import           Data.Composition
import           Data.Functor
import           Data.List
import qualified Data.Map           as M
import           Data.Maybe
import           Data.Tuple.HT

import           Safe
import           System.Environment

import           Text.Printf


newtype Cfg = Cfg { cfgMap :: M.Map String String }


instance Show Cfg where
    show (Cfg cfgMap) = unlines $ "Cfg:":map showKV (M.toList cfgMap)
        where
            showKV :: (String,String) -> String
            showKV (k,v) = pad k++" = "++v

            pad :: String -> String
            pad = printf ("    % "++show padLen++"s")

            padLen :: Int
            padLen = maximum . map (length . fst) $ M.toList cfgMap

loadCfg :: IO Cfg
loadCfg = Cfg . M.fromList <$> ((++)<$>getCfgFileKVs<*>getArgKVs)
    where
        getArgKVs :: IO [(String,String)]
        getArgKVs = toKeyVals . args<$>getArgs
            where
                toKeyVals :: [String] -> [(String,String)]
                toKeyVals (('-':'-':x):xs)
                    | null xs                = [(x,"")]
                    | take 2 (head xs)=="--" =  (x,"")         : toKeyVals xs
                    | otherwise              =  (x,unwords ks) : toKeyVals xs'
                    where (ks,xs') = break ((=="--") . take 2) xs
                toKeyVals (x:xs)             =                   toKeyVals xs
                toKeyVals []                 = []

                args :: [String] ->  [String]
                args []           = []
                args (x:xs)
                    | head x=='"' = x':xs'
                    | otherwise   = x :args xs
                    where
                        (x',xs') = (\(xs,y:ys) -> (unwords $ x:xs++[y],ys))
                                 $ break ((=='"') . last) xs


        getCfgFileKVs :: IO [(String,String)]
        getCfgFileKVs = (cfgFp>>=readFile)<&>parseCfgFile

        cfgFp :: IO FilePath
        cfgFp = headDef defCfgFp . tailSafe . dropWhile (/="--cfg")<$>getArgs
            where defCfgFp = "dump-distribution.cfg"

        parseCfgFile :: FilePath -> [(String,String)]
        parseCfgFile = map (mapPair (trim,trim . tail) . break (=='='))
                     . filter (elem '=') . map removeComments . lines

        removeComments :: String -> String
        removeComments            ""  = ""
        removeComments ('\\':'\\':xs) = '\\':removeComments xs
        removeComments ('\\':'#' :xs) = '#' :removeComments xs
        removeComments (     '#' :xs) = ""
        removeComments (      x  :xs) =  x  :removeComments xs

        trim :: String -> String
        trim = reverse . dropWhile isSpace . reverse . dropWhile isSpace


insertCfg :: String -> String -> Cfg -> Cfg
insertCfg k v = Cfg . M.insert k v . cfgMap


getReadOption :: Read a => String -> Cfg -> a
getReadOption = read.:getOption

getReadOptionSafe :: Read a => String -> Cfg -> Maybe a
getReadOptionSafe = (<$>)read.:getOptionSafe

getOption :: String -> Cfg -> String
getOption k = fromMaybe (error $ "option "++k++" not passed") . getOptionSafe k

getOptionSafe :: String -> Cfg -> Maybe String
getOptionSafe k = M.lookup k . cfgMap
