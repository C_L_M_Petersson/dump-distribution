{-# LANGUAGE ScopedTypeVariables #-}
module Output where

import           Configure
import           Control.Lens
import           Control.Monad

import           Data.List

import           Geometry

import           Projection

import           MolDyn
import           MolDyn.Dump
import           MolDyn.Dump.Utils
import           MolDyn.Treatment.Distribution
import           MolDyn.Treatment.Layers


putStr' :: String -> MolDyn ()
putStr' = liftIO . putStr

putStrLn' :: String -> MolDyn ()
putStrLn' "" = return()
putStrLn' l  = putStr'(l++"\n")

print' :: Show a => a -> MolDyn ()
print' = putStrLn' . show


show1DDists :: forall a. Show a => MolDyn [a] -> [[a]] -> MolDyn String
show1DDists getXs ys = unlines<$>(show1DDistsRec<$>dumpTime<*>getXs??ys)
    where
        show1DDistsRec :: Double -> [a] -> [[a]] -> [String]
        show1DDistsRec t (x:xs) (ys:yss) = unwords (show t:map show (x:ys))
                                         : show1DDistsRec t xs yss
        show1DDistsRec _  _      _       = []

showXDists :: [[Double]] -> MolDyn String
showXDists = show1DDists getXRange

showYDists :: [[Double]] -> MolDyn String
showYDists = show1DDists getYRange

showAngDists :: [[Double]] -> MolDyn String
showAngDists = show1DDists (map (*(180/pi))<$>getAngRange)

showQDists :: [[Double]] -> MolDyn String
showQDists = show1DDists getQRange

showQ'Dists :: [[Double]] -> MolDyn String
showQ'Dists = show1DDists getQ'Range


show2DDists :: MolDyn [Double] -> MolDyn [Double] -> [[[Double]]]
                                                                -> MolDyn String
show2DDists getXs getYs = (concat<$>) . (getYs>>=) . zipWithM (flip show2DDist)
    where
        show2DDist :: Double -> [[Double]] -> MolDyn String
        show2DDist y zss = unlines . ("":)
                         . map (unwords . (\(t:ws) -> t:show y:ws) . words)
                         . lines<$>show1DDists getXs zss

showQQ'Dists :: [[[Double]]] -> MolDyn String
showQQ'Dists = show2DDists getQRange getQ'Range


showMaxima :: [Dump] -> MolDyn String
showMaxima ds = unwords . map show<$>
    ((:)<$>dumpTime<*>mapM (withDump dumpDistributionMaximum) ds)

showAtomsPerCells :: [Dump] -> MolDyn String
showAtomsPerCells ds = unwords . map show <$>
    ((:)<$>dumpTime<*>mapM (withDump atomsPerCell) ds)

showImage2DDists :: (Direction,Direction,Direction) -> [[[Double]]]
                                                                -> MolDyn String
showImage2DDists (dir,xDir,yDir) dist = (<$>) unlines . join
    $ zipWithM
        (\x d -> unlines . map (extractDirAndTime x) . lines<$>showYDists d)
        <$>getXRange??transpose dist
    where extractDirAndTime x vs = let t:vs' = words vs
                                    in unwords
                                     $ show dir:show xDir:show yDir:t:show x:vs'

showXAngDists :: [[[Double]]] -> MolDyn String
showXAngDists dist = (<$>) unlines . join
    $ zipWithM (\x d -> unlines . map (extractTime x) . lines<$>showAngDists d)
        <$>getXRange??transpose dist
    where extractTime x ys = let t:ys' = words ys in unwords $ t:show x:ys'
