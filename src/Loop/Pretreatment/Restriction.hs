module Loop.Pretreatment.Restriction where

import           Data.Char
import           Data.Tuple.HT


data Restriction = RTree
                   { andOr  :: Bool -> Bool -> Bool
                   , leftR  :: Restriction
                   , rightR :: Restriction
                   }
                 | Always
                 | Never
                 | Poly
                   { op     :: Double -> Double -> Bool
                   , coeffs :: [[Double]]
                   }

instance Read Restriction where
    readsPrec _ str
        | isRTree   = [(readTree 0 "" str',"")]
        | otherwise = case w of
            "Poly"   -> [(readPoly ws,"")]
            "Always" -> [(Always,"")]
            "Never"  -> [(Never ,"")]
        where
            str' = reverse . dropWhile isSpace . reverse $ dropWhile isSpace str
            (w:ws) = words str'
            isRTree = '|'`elem`str'||'&'`elem`str'

            readTree :: Int -> String -> String -> Restriction
            readTree 0 hs ('&':'&':ts) = RTree (&&) (subRead hs) (subRead ts)
            readTree 0 hs ('|':'|':ts) = RTree (||) (subRead hs) (subRead ts)
            readTree i hs ('('    :ts) = readTree (i+1) (hs++"(") ts
            readTree 0 hs (')'    :ts) = readTree  0    (hs++")") ts
            readTree i hs (')'    :ts) = readTree (i-1) (hs++")") ts
            readTree i hs ( t     :ts) = readTree  i    (hs++[t]) ts

            subRead :: String -> Restriction
            subRead str = read str'
                where str' | head str=='(' = init $ tail str
                           | otherwise     = str


            readPoly :: [String] -> Restriction
            readPoly (w:ws) = Poly op (map read ws)
                where op | w=="<" ||w=="(<)"  = (<)
                         | w==">" ||w=="(>)"  = (>)
                         | w=="=="||w=="(==)" = (==)
                         | w==">="||w=="(>=)" = (>=)
                         | w=="<="||w=="(<=)" = (<=)


inArea :: Restriction -> [Double] -> Double -> Bool
inArea (RTree andOr lR rR) xs y = inArea lR xs y`andOr`inArea rR xs y
inArea  Always             _  _ = True
inArea  Never              _  _ = False
inArea (Poly  op coeffs  ) xs y = y`op`sum (zipWith (getPolyn 1) xs coeffs)
    where
        getPolyn :: Double -> Double -> [Double] -> Double
        getPolyn xN x (a:as) = xN*a + getPolyn (x*xN) x as
        getPolyn _  _ _      = 0
