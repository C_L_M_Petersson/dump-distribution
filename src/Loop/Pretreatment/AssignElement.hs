--{-# LANGUAGE FlexibleInstances #-}
--{-# LANGUAGE TypeSynonymInstances #-}
module Loop.Pretreatment.AssignElement where

import           MolDyn.Atom
import           MolDyn.Atom.Kind
import           MolDyn.Dump
import           MolDyn.Dump.Utils


type ElemDict = [(Kind,String)]

elemDict :: [String] -> ElemDict
elemDict = zip $ kindList AllKinds


assignDumpElements :: ElemDict -> Dump -> Dump
assignDumpElements ed d = mapDumpAtoms (assignAtomElement ed)
    d{ atomsLine = take 2 (atomsLine d)++["element"]++drop 2 (atomsLine d) }

assignAtomElement :: ElemDict -> Atom -> Atom
assignAtomElement ed a = a{ element = Just . snd . head
                                    $ filter ((==kind a) . fst) ed'
                          }
    where ed' = ed++[(kind a,error
              $ "kind "++show (kind a)++" does not correspond to any element")]
