{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase        #-}
module Loop.Pretreatment where

import           Control.Lens.Lens

import           Data.Composition
import           Data.List.Extra

import           Geometry

import           Loop.Pretreatment.AssignElement
import           Loop.Pretreatment.Restriction

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Atom.Kind
import           MolDyn.Dump
import           MolDyn.Dump.Utils
import           MolDyn.Treatment.Angular

type Treatment = [Dump] -> MolDyn [Dump]

instance Read Treatment where
    readsPrec _ str = [(t,"")]
        where t = case words str of
                ["ConvertAtomKind",ks,k] -> treatConvertAtomKind (read ks)
                                                                 (read k)
                ["Head"]                 -> treatHead
                ["Take",i]               -> treatTake (read i)
                ["Drop",i]               -> treatDrop (read i)
                ["Last"]                 -> treatLast
                ["EveryNth",i]           -> treatEveryNth (read i)
                ["Mirror",p]             -> treatMirror (read p)
                ["Merge"]                -> treatMerge
                ["WithinOf",r,k]         -> treatWithinOf (read r) (read k)
                ["CenterKind",k]         -> treatCenterKind (read k)
                ("XRestriction":rs)      -> treatXRestriction
                                                (read $ unwords rs)
                ("YRestriction":rs)      -> treatYRestriction
                                                (read $ unwords rs)
                ("ZRestriction":rs)      -> treatZRestriction
                                                (read $ unwords rs)
                ("AssignElements":ed)    -> treatAssignElements (elemDict ed)
                strs                     -> error $ "pretreatment option "
                                                 ++ unwords strs
                                                 ++ " not recognised"


applyAllTreatments :: MolDyn ()
applyAllTreatments = mapM (getTreatments 0) ["Insert","","Append"]
                        >>=applyTreatments . concat
    where
        getTreatments :: Int -> String -> MolDyn [Treatment]
        getTreatments i p = getReadOptionSafe ("treatment"++p++show i)>>= \case
            Nothing -> return[]
            Just t  -> (t:)<$>getTreatments (i+1) p

applyTreatments :: [Treatment] -> MolDyn ()
applyTreatments = mapM_ applyTreatment

applyTreatment :: Treatment -> MolDyn ()
applyTreatment = modifyDumpsM


treatConvertAtomKind :: Kind -> Kind -> Treatment
treatConvertAtomKind k k' = return . map (mapDumpAtoms convertAtomKind)
    where
        convertAtomKind :: Atom -> Atom
        convertAtomKind a
            | kind a==k = a{ kind = k' }
            | otherwise = a

treatHead :: Treatment
treatHead = return . return . head

treatTake :: Int -> Treatment
treatTake = return.:take

treatDrop :: Int -> Treatment
treatDrop = return.:drop

treatLast :: Treatment
treatLast = return . return . last

treatEveryNth :: Int -> Treatment
treatEveryNth i = return . map snd . filter fst
                . zip (cycle (True:replicate (i-1) False))

treatMirror :: Double -> Treatment
treatMirror x0 = mapM (mapDumpAtoms<$>(mirrorAtom  <$>getOption "dim")??)
    where
        mirrorAtom :: String -> Atom -> Atom
        mirrorAtom "x" = fromToX mirrorCoordinate
        mirrorAtom "y" = fromToY mirrorCoordinate
        mirrorAtom "z" = fromToZ mirrorCoordinate

        mirrorCoordinate :: Double -> Double
        mirrorCoordinate x
            | x<x0      = x0-x
            | otherwise = x

treatMerge :: Treatment
treatMerge = ((:[])<$>) . mergeDumps

treatWithinOf :: Double -> Kind -> Treatment
treatWithinOf dx k = return . map dumpFilter
    where
        dumpFilter :: Dump -> Dump
        dumpFilter d = filterDump
            (flip atomFilter . filter ((k==) . kind) $ atoms d) d

        atomFilter :: Atom -> [Atom] -> Bool
        atomFilter a = notNull . filter (within dx a)

treatCenterKind :: Kind -> Treatment
treatCenterKind k = return . map (\d -> shift (-center d)`mapDumpAtoms`d)
    where
        center :: Dump -> Coordinate
        center = averagePos . filter ((==k) . kind) . atoms

treatXRestriction :: Restriction -> Treatment
treatXRestriction r = return . map (filterDump $ \a -> inArea r [y a,z a] (x a))

treatYRestriction :: Restriction -> Treatment
treatYRestriction r = return . map (filterDump $ \a -> inArea r [x a,z a] (y a))

treatZRestriction :: Restriction -> Treatment
treatZRestriction r = return . map (filterDump $ \a -> inArea r [x a,y a] (z a))

treatAssignElements :: ElemDict -> Treatment
treatAssignElements ed = return . map (assignDumpElements ed)
