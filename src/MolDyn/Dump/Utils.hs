{-# LANGUAGE MultiWayIf #-}
module MolDyn.Dump.Utils where

import           Control.Lens.Lens
import           Control.Monad.Extra
import           Control.Monad.ListM
import           Control.Monad.State

import           Data.List
import           Data.List.Split

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Atom.Kind
import           MolDyn.Dump


mapDumpAtoms :: (Atom -> Atom) -> Dump -> Dump
mapDumpAtoms f d = d{ atoms = map f $ atoms d }

changeDumpAtoms :: ([Atom] -> [Atom]) -> Dump -> Dump
changeDumpAtoms f d = d{ atoms = f $ atoms d }


atomKindDump :: Kind -> MolDyn Dump
atomKindDump k = filterDump ((==k) . kind)<$>getDump

metalDump  :: MolDyn Dump
metalDump  = getReadOption "metalAtom" >>=atomKindDump

oxygenDump :: MolDyn Dump
oxygenDump = getReadOption "oxygenAtom">>=atomKindDump

filterDump :: (Atom -> Bool) -> Dump -> Dump
filterDump f d = d{ atoms = f`filter`atoms d }

partitionDump :: (Atom -> Bool) -> Dump -> (Dump,Dump)
partitionDump f d = ( d{ atoms = fst $ f`partition`atoms d }
                    , d{ atoms = snd $ f`partition`atoms d }
                    )

mergeDumps :: [Dump] -> MolDyn Dump
mergeDumps ds = (\d -> d{ atoms = foldr (union . atoms) [] ds })<$>getDump


dumpAverage :: Dump -> MolDyn Double
dumpAverage = ((\as -> sum as/genericLength as)<$>)
            . mapM ((inDir<$>getReadOption "dim")??) . atoms

dumpTime :: MolDyn Double
dumpTime = (*) . fromIntegral<$>getsDump timeStep<*>getReadOption "dt"


sortDumpBy :: (Atom -> Atom -> MolDyn Ordering) -> Dump -> MolDyn Dump
sortDumpBy f d = (\as -> d{ atoms = as })<$>sortByM f (atoms d)


withLargestDim :: ((Atom -> Double) -> a) -> MolDyn a
withLargestDim f = getDump >>= \d ->
    let xLen = flip (-)`uncurry`xLims d
        yLen = flip (-)`uncurry`yLims d
        zLen = flip (-)`uncurry`zLims d
     in return $ f if
        | xLen>yLen&&xLen>zLen -> x
        | yLen>zLen            -> y
        | otherwise            -> z

withinLargestDim :: String -> Atom -> [Atom] -> MolDyn ([Atom],[Atom])
withinLargestDim maxOpt a as = do
    xMax   <- getReadOption maxOpt
    asTail <- dropWhileM (withLargestDim (\dim -> ((dim a-xMax)>).dim)??) as
    asHead <- takeWhileM (withLargestDim (\dim -> ((dim a+xMax)>).dim)??) asTail
    return (asHead,asTail)

compareLargestDim :: Atom -> Atom -> MolDyn Ordering
compareLargestDim a a' = withLargestDim (\dim -> dim a`compare`dim a')

sortedKindDump :: Kind -> MolDyn Dump
sortedKindDump k = getDump>>=sortDumpBy compareLargestDim
                                . filterDump ((==k) . kind)
