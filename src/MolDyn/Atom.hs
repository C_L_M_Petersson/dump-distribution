{-# LANGUAGE FlexibleInstances #-}
module MolDyn.Atom where

import           Data.List
import           Data.Maybe
import           Data.Tuple

import           Geometry

import           MolDyn.Atom.Kind

import           Text.Printf


data Atom = Atom
            { index    :: Int
            , element  :: Maybe String
            , kind     :: Kind
            , atomPos  :: Coordinate
            , atomData :: [(String,Double)]
            } deriving(Show)

instance Eq Atom where
    a==a' = index a==index a'

instance Positioned Atom where
    pos    = atomPos
    to a c = a{ atomPos = c }


compareAtomIndex :: Atom -> Atom -> Ordering
a`compareAtomIndex`a' = kind a`compare`kind a'

getAtomData :: String -> Atom -> Double
getAtomData k a
    | isJust mv = fromJust mv
    | otherwise = error $ "dump does not contain column "++k
    where mv = snd<$>find ((==k) . fst) (atomData a)
