module MolDyn.Treatment.Distribution where

import           Control.Lens.Lens
import           Control.Monad

import           Data.List
import           Data.Tuple.Extra

import           Geometry

import           Projection

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Atom.Kind
import           MolDyn.Dump
import           MolDyn.Dump.Utils


dumpAtomKindPosList :: Kind -> MolDyn [Double]
dumpAtomKindPosList k = getReadOption "dim">>=(\xf ->
    map xf . atoms . filterDump ((==k) . kind)<$>getDump) . inDir

dumpAtomKindDist :: Kind -> MolDyn [Double]
dumpAtomKindDist k = getXRange>>= \xs ->
    dumpAtomKindPosList k>>=getSmooth . derivate xs . integrate xs

dumpAtomKindDists :: MolDyn [[Double]]
dumpAtomKindDists = getReadOption "atomKinds">>=mapM dumpAtomKindDist . kindList

dumpTotalDist :: MolDyn [Double]
dumpTotalDist = map sum . transpose<$>dumpAtomKindDists


oxygenDist :: MolDyn [Double]
oxygenDist = oxygenDump>>=withDump dumpTotalDist

metalDist :: MolDyn [Double]
metalDist = metalDump>>=withDump dumpTotalDist


dumpDistributionMaximum :: MolDyn Double
dumpDistributionMaximum = dumpTotalDist>>=distributionMaximum
    where
        distributionMaximum :: [Double] -> MolDyn Double
        distributionMaximum ps = uncurry (/) . both sum . unzip
                               . map (\(x,y) -> (x*y**2,y**2))
                               . filter ((>0.5*maximum ps) . snd)
                                <$>(zip<$>getXRange??ps)


image2D :: MolDyn [((Direction,Direction,Direction),[[[Double]]])]
image2D = do
    dirs <- map directions<$>getReadOption "viewDir"
    vals <- mapM (uncurry3 image2DInDir) dirs

    return $ zip dirs vals
    where
        directions :: Direction -> (Direction,Direction,Direction)
        directions dir = (dir,xDir,yDir)
            where
                xDir :: Direction
                xDir| dir==Dir 0 0   1  = Dir 0   1  0
                    | dir==Dir 0 0 (-1) = Dir 0 (-1) 0
                    | otherwise         = dir`to`(dir><Dir 0 0 1)

                yDir :: Direction
                yDir = dir`to`(xDir><dir)

image2DInDir :: Direction -> Direction -> Direction -> MolDyn [[[Double]]]
image2DInDir dir xDir yDir = getReadOption "atomKinds">>=
    mapM (atomKindDump>=>image2DInDirWithKind dir xDir yDir) . kindList

image2DInDirWithKind :: Direction -> Direction -> Direction -> Dump
                                                            -> MolDyn [[Double]]
image2DInDirWithKind dir xDir yDir d = dists (atoms d)<$>getXRange<*>getYRange
                                            >>=mapM getSmooth>>=getOuterSmooth
    where
        xPos :: Atom -> Double
        xPos a = a<|>xDir

        yPos :: Atom -> Double
        yPos a = a<|>yDir


        dists ::  [Atom] -> [Double] -> [Double] -> [[Double]]
        dists as xs ys = xyDist as' xs ys
            where
                as' = sortOn xPos . excludeOutside yPos ys
                                  $ excludeOutside xPos xs as

        xyDist :: [Atom] -> [Double] -> [Double] -> [[Double]]
        xyDist as [_]       ys = [yDist (yPos`sortOn`as ) ys]
        xyDist as (x:x':xs) ys =  yDist (yPos`sortOn`ash) ys
                                : xyDist ast (x':xs) ys
            where (ash,ast) = break ((>(x+x')/2) . xPos) as

        yDist :: [Atom] -> [Double] -> [Double]
        yDist as [_]       = [genericLength as ]
        yDist as (y:y':ys) =  genericLength ash : yDist ast (y':ys)
            where (ash,ast) = break ((>(y+y')/2) . yPos) as

        excludeOutside :: (Atom -> Double) -> [Double] -> [Atom] -> [Atom]
        excludeOutside dir xs = filter isInside
            where
                dx   = (last xs-head xs)/(genericLength xs-1)
                isInside :: Atom -> Bool
                isInside a
                    | dir a<head xs-dx/2 = False
                    | dir a>last xs+dx/2 = False
                    | otherwise          = True
