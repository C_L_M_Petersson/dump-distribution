module MolDyn.Treatment.Radial where

import           Control.Lens
import           Control.Monad
import           Control.Monad.Loops

import           Data.Foldable
import           Data.Functor
import           Data.List

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Atom.Kind
import           MolDyn.Dump
import           MolDyn.Dump.Utils
import           MolDyn.Treatment.Distribution

import           Projection


radialHeader :: Bool -> MolDyn String
radialHeader restrict = unwords . ("#t":) . ("r":) .
    (\aks -> [ show i++"-"++show j | i <- aks, j <- aks, not restrict||i<=j
                          ]) . kindList<$>getReadOption "atomKinds"


radial :: Bool -> Dump -> Dump -> MolDyn [[Double]]
radial restrict d d' = getReadOption "atomKinds">>=(\aks ->
        mapM ((\(di,dj) -> join $ radialij<$>di<*>dj)>=>getSmooth)
        [ (sortedKindDump i`withDump`d,sortedKindDump j`withDump`d')
                                                    | i <- aks, j <- aks
                                                    , not restrict||i<=j
                                                    ]) . kindList

radialij :: Dump -> Dump -> MolDyn [Double]
radialij di dj = normalise
        <$>join (radialRec<$>getXRange??repeat 0??atoms di??atoms dj)
    where
        normalise :: [Double] -> [Double]
        normalise
            | null (atoms di) = map (const 0)
            | otherwise       = map (/genericLength (atoms di))

        radialRec :: [Double] -> [Double] -> [Atom] -> [Atom] -> MolDyn [Double]
        radialRec _  ys []       _   = return ys
        radialRec xs ys (ai:ais) ajs = join $ (radialRec xs<$>ys'??ais)
                                                <*>(snd<$>ajs')
            where
                ys'  = radialSingle xs ys ai . filter (/=ai) . fst<$>ajs'
                ajs' = withinLargestDim "xMax" ai ajs

        radialSingle :: [Double] -> [Double] -> Atom -> [Atom] -> [Double]
        radialSingle []     _     _  _    = []
        radialSingle (x:xs) (y:ys) ai ajs = y+genericLength ajsh
                                          : radialSingle xs ys ai ajst
            where (ajsh,ajst) = partition (within x ai) ajs
