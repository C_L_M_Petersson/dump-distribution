module MolDyn.Treatment.Layers where

import           Control.Lens.Lens

import           Data.List
import           Data.Tuple.HT

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Dump
import           MolDyn.Dump.Utils


oxygenLayers :: [Dump] -> MolDyn [Dump]
oxygenLayers ps = oxygenDump>>=withDump (oxygenLayersRec ps)
    where
        oxygenLayersRec :: [Dump] -> MolDyn [Dump]
        oxygenLayersRec [p]    = (:[])<$>getDump
        oxygenLayersRec (p:ps) = belowLayer p>>=(<$>getDump) . partitionDump
            >>=uncurry (<$>) . mapPair ((:),withDump (oxygenLayersRec ps))

        belowLayer :: Dump -> MolDyn (Atom -> Bool)
        belowLayer p = (.)<$>((>)<$>dumpAverage p)
                          <*>(inDir<$>getReadOption "dim")


atomsPerCell :: MolDyn Double
atomsPerCell = (/)<$>getsDump (genericLength . atoms)
                  <*>(uncurry (*)<$>getReadOption "xyCells")
