module MolDyn.Treatment.Charge where

import           Control.Lens
import           Control.Monad

import           Data.List

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Atom.Kind
import           MolDyn.Dump
import           MolDyn.Dump.Utils

import           Projection

import           Text.Printf



chargeWithinHeader :: Bool -> MolDyn String
chargeWithinHeader restrict = unwords . ("#t":) . ("r":) .
    (\aks -> [ show i++"-"++show j | i <- aks, j <- aks, not restrict||i<=j
                          ]) . kindList<$>getReadOption "atomKinds"


chargeWithin :: Bool -> MolDyn [[[Double]]]
chargeWithin restrict = do
    d   <- getDump
    dQs <- dumpsByCharge d<$>getQRange

    transpose<$>(sequence [ chargeWithinQ restrict dQ d | dQ <- dQs ]
                    >>=getOuterSmooth>>=getInnerSmooth 1)

chargeWithinQ :: Bool -> Dump -> Dump -> MolDyn [[Double]]
chargeWithinQ restrict dQ d = do
    dQs' <- dumpsByCharge d<$>getQ'Range

    sequence [ chargeWithinQQ' restrict dQ dQ' | dQ' <- dQs' ]

chargeWithinQQ' :: Bool -> Dump -> Dump -> MolDyn [Double]
chargeWithinQQ' restrict dQ dQ' = getReadOption "atomKinds">>=(\aks ->
    sequence [ chargeWithinijQKQ'K' (filterDump ((==k ) . kind) dQ )
                                    (filterDump ((==k') . kind) dQ')
             | k <- aks, k' <- aks, not restrict||k<=k' ]) . kindList

chargeWithinijQKQ'K' :: Dump -> Dump -> MolDyn Double
chargeWithinijQKQ'K' dQK dQ'K' = sum<$>mapM (`chargeWithinijQKSingleQ'K'`dQ'K')
                                            (atoms dQK)

chargeWithinijQKSingleQ'K' :: Atom -> Dump -> MolDyn Double
chargeWithinijQKSingleQ'K' aQK dQ'K' = genericLength
    <$>mapM ((within<$>getReadOption "bondMax"??aQK)??) (atoms dQ'K')


atomsByCharge :: [Double] -> [Atom] -> [[Atom]]
atomsByCharge = atomsByCharge' . rangeBetweenSteps
    where
        atomsByCharge' :: [Double] -> [Atom] -> [[Atom]]
        atomsByCharge'  []    as = [as]
        atomsByCharge' (q:qs) as =  ash : atomsByCharge' qs ast
            where (ash,ast) = partition ((<q) . getAtomData "q") as

dumpsByCharge :: Dump -> [Double] -> [Dump]
dumpsByCharge d qs = map ((`changeDumpAtoms`d) . const)
                         (qs`atomsByCharge`atoms d)
