module MolDyn.Treatment.Depth where

import           Control.Lens.Lens
import           Control.Monad.ListM

import           Data.List

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Dump
import           MolDyn.Dump.Utils


giveDepths :: MolDyn [Maybe Double]
giveDepths = do
    (oZsLo,oZsHi) <- oxygenDump>>=splitAtomDist . atoms
    (mZsLo,mZsHi) <- metalDump >>=splitAtomDist . atoms

    mapM getNthAtom [mZsLo,reverse mZsHi,reverse oZsLo,oZsHi]
    where
        splitAtomDist :: [Atom] -> MolDyn ([Double],[Double])
        splitAtomDist as = (break . (<)<$>getReadOption "xCenter")
                            <*>(sort<$>(map . inDir<$>getReadOption "dim"??as))

        getNthAtom :: [Double] -> MolDyn (Maybe Double)
        getNthAtom [] = return Nothing
        getNthAtom zs = Just . head . (`drop`zs)<$>getReadOption "atomDepthN"
