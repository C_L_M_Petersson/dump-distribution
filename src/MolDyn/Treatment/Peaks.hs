module MolDyn.Treatment.Peaks where

import           Control.Lens.Lens
import           Control.Monad

import           Data.Foldable
import           Data.Functor

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Dump
import           MolDyn.Dump.Utils
import           MolDyn.Treatment.Distribution

import           Projection


getPeaks :: MolDyn [Dump]
getPeaks = join . ((flip withDump<$>metalDump)??) $ mapM peakToDump
    =<<join (atomsToPeak<$>(peakPositions=<<metalDist)<*>getsDump atoms)
    where
        peakPositions :: [Double] -> MolDyn [Double]
        peakPositions aps = ($aps) . peakPositionsRec<$>getXRange
            where
                half :: Double
                half = maximum aps/2

                peakPositionsRec :: [Double] -> [Double] -> [Double]
                peakPositionsRec (x:x':x'':xs) (a:a':a'':as)
                    | a' < half      =    tails
                    | a' < a         =    tails
                    | a' < a''       =    tails
                    | otherwise      = x':tails
                    where tails      = peakPositionsRec (x':x'':xs) (a':a'':as)
                peakPositionsRec _ _ = []

        atomsToPeak :: [Double] -> [Atom] -> MolDyn [[Atom]]
        atomsToPeak pxs = foldlM (atomToPeak pxs) (replicate (length pxs) [])
            where
                atomToPeak ::  [Double] -> [[Atom]] -> Atom
                                        -> MolDyn [[Atom]]
                atomToPeak _            [pa]     a = return [a:pa]
                atomToPeak (px:px':pxs) (pa:pas) a =
                        (inDir<$>getReadOption "dim")??a
                    >>= \x -> if x<0.5*(px+px')
                                    then return $ (a:pa):pas
                                    else (pa:) <$> atomToPeak (px':pxs) pas a

        peakToDump :: [Atom] -> MolDyn Dump
        peakToDump as = getsDump $ \d -> d{ atoms = as }

        metalAtoms :: MolDyn [Atom]
        metalAtoms = atoms <$> metalDump


evolvePeaks :: [Dump] -> MolDyn [Dump]
evolvePeaks = mapM evolvePeak
    where
        evolvePeak :: Dump -> MolDyn Dump
        evolvePeak p = getsDump $ \d -> d{ atoms = evolvedAtoms d p }

        evolvedAtoms :: Dump -> Dump -> [Atom]
        evolvedAtoms d = map ((\j -> head$filter ((==j).index) (atoms d)).index)
                       . atoms
