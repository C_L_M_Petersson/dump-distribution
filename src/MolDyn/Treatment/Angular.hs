module MolDyn.Treatment.Angular where

import           Control.Lens.Lens
import           Control.Monad
import           Control.Monad.Extra

import           Data.Composition
import           Data.Function
import           Data.Function.Slip
import           Data.Functor
import           Data.List
import           Data.Maybe

import           Geometry

import           MolDyn
import           MolDyn.Atom
import           MolDyn.Dump
import           MolDyn.Dump.Utils

import           Projection


cosAngDist :: MolDyn [[[Double]]]
cosAngDist = withCfg (withCfg cosAngRadDist "xN" "2") "xMin" "0"

cosAngRadDist :: MolDyn [[[Double]]]
cosAngRadDist = getDump>>= \d ->
    sequence [getReadOption "oxygenAtom",getReadOption "metalAtom"]>>= \aks ->
    sequence [sequence [ sortedKindDump p`withDump`d
                       , sortedKindDump i`withDump`d
                       , sortedKindDump j`withDump`d
                       ] | p <- aks, i <- aks, j <- aks
             ]>>=mapM (\[d',di,dj] -> cosAngRadDistij di dj`withDump`d')

cosAngRadDistij :: Dump -> Dump -> MolDyn [[Double]]
cosAngRadDistij di dj = ifM (null<$>getsDump atoms) cosAngRadDistijNull
                      $ getsDump atoms
                            >>=slipr cosAngRadDistijRec (atoms di) (atoms dj)
                                <&>foldr1 (zipWith $ zipWith (+))
    where
        cosAngRadDistijNull :: MolDyn [[Double]]
        cosAngRadDistijNull = replicateM<$>getReadOption "angN"
                           <*>(replicate<$>getReadOption "xN"??0)

        cosAngRadDistijRec :: [Atom] -> [Atom] -> [Atom] -> MolDyn [[[Double]]]
        cosAngRadDistijRec    []  ais ajs = return []
        cosAngRadDistijRec (a:as) ais ajs = do
            (aisHead,aisTail) <- withinLargestDim "xMax" a ais
            (ajsHead,ajsTail) <- withinLargestDim "xMax" a ajs

            (:)<$>cosAngRadDistAij   a  aisHead ajsHead
               <*>cosAngRadDistijRec as aisTail ajsTail

cosAngRadDistAij :: Atom -> [Atom] -> [Atom] -> MolDyn [[Double]]
cosAngRadDistAij a ais ajs = (join>=>getOuterSmooth)
                           $  cosAnglesByRad<$>aRAngs
                           <*>((flip zip . rangeTail<$>getXRange)<*>getXRange)
                           <*>getCosAngRange
    where
        filterAs as = (`filter`as) . (`within`a)<$>getReadOption "xMax"
        aRAngs = sortBy (compare`on`fst).:radCosAngleCombs a
                    <$>filterAs ais<*>filterAs ajs

        cosAnglesByRad :: [(Double,Double)] -> [Range] -> [Double]
                            -> MolDyn [[Double]]
        cosAnglesByRad _      []               _    = return []
        cosAnglesByRad aRAngs ((rMin,rMax):rs) angs = (:)<$>cAHead<*>cATail

            where
                  cAHead = getSmooth $ groupCosAngles
                                (sortBy (flip compare) $ map snd aRAngs') angs
                  cATail = cosAnglesByRad aRAngs'' rs angs
                  (aRAngs',aRAngs'') = span      ((<rMax) . fst)
                                     $ dropWhile ((<rMin) . fst) aRAngs

        groupCosAngles :: [Double] -> [Double] -> [Double]
        groupCosAngles _      []     = []
        groupCosAngles aRAngs (c:cs) = genericLength aRAngs'
                                     : groupCosAngles aRAngs'' cs
            where
                (aRAngs',aRAngs'') = span (>c) aRAngs


radCosAngleCombs :: Atom -> [Atom] -> [Atom] -> [(Double,Double)]
radCosAngleCombs _ []       _   = []
radCosAngleCombs a (ai:ais) ajs = mapMaybe (radCosAngleComb a ai) ajs
                                    ++radCosAngleCombs a ais ajs

radCosAngleComb :: Atom -> Atom -> Atom -> Maybe (Double,Double)
radCosAngleComb a ai aj
    | ai==aj || a==ai || a==aj = Nothing
    | otherwise                = Just (dist a ai,cosAngle a ai aj)
