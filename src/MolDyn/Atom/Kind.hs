module MolDyn.Atom.Kind
(   Kind(AllKinds)

,   kindList
,   kindInts
) where

import           Data.List
import           Data.List.Split
import           Data.List.Unique


data Kind = Kind  { kind  ::  Int  }
          | Kinds { kinds :: [Int] }
          | AllKinds

instance Eq Kind where
    Kind  k ==Kind  k'  = k==k'
    Kinds ks==Kinds ks' = any (`elem`ks) ks
    Kind  k ==Kinds ks' = k `elem`ks'
    Kinds ks==Kind  k'  = k'`elem`ks
    AllKinds==_         = True
    _       ==AllKinds  = True

instance Ord Kind where
    Kind  k `compare`Kind  k'  = k `compare`k'
    Kinds ks`compare`Kinds ks' = ks`compare`ks'

instance Read Kind where
    readsPrec _ str
        | str=="*"     = [(AllKinds,"")]
        | '|'`elem`str = [(Kinds . sortUniq . map read $ splitOn "||" str,"")]
        | otherwise    = [(Kind $ read str,"")]

instance Show Kind where
    show (Kind  k ) = show k
    show (Kinds ks) = intercalate "||" $ map show ks
    show AllKinds   = "*"


kindList :: Kind -> [Kind]
kindList (Kind  k ) = [Kind k]
kindList (Kinds ks) = map Kind ks
kindList AllKinds   = map Kind [1..]

kindInts :: Kind -> [Int]
kindInts = map kind . kindList
