module MolDyn.Dump where

import           Configure

import           Data.List
import           Data.List.Split
import           Data.Maybe
import           Data.Tuple.HT

import           Geometry

import           MolDyn.Atom

import           Safe

import           Text.Printf


data Dump = Dump
            { timeStep   :: Int
            , boundsLine :: [String]
            , xLims      :: (Double,Double)
            , yLims      :: (Double,Double)
            , zLims      :: (Double,Double)
            , atomsLine  :: [String]
            , atoms      :: [Atom]
            }

instance Show Dump where
    show d = unlines
           . ([ "ITEM: TIMESTEP"
              , show $ timeStep d
              , "ITEM: NUMBER OF ATOMS"
              , show . length $ atoms d
              , "ITEM: BOX BOUNDS "++unwords (boundsLine d)
              , (show . fst $ xLims d)++" "++(show . snd $ xLims d)
              , (show . fst $ yLims d)++" "++(show . snd $ yLims d)
              , (show . fst $ zLims d)++" "++(show . snd $ zLims d)
              , "ITEM: ATOMS "++unwords (atomsLine d)
              ]++) . zipWith (showInDump d) [1..] $ atoms d

instance Read Dump where
    readsPrec _ str = [(dumpHead atoms,"")]
        where
            dumpHead   = Dump
                            timeStep
                            boundsLine xLims yLims zLims
                            atomsLine
            strLines   = lines str

            timeStep   = read (strLines!!1)

            boundsLine = drop 3 $ words (strLines!!4)
            lims i     = (\[min,max] -> (min,max)) . map read
                                                   $ words (strLines!!i)
            xLims      = lims 5
            yLims      = lims 6
            zLims      = lims 7

            atomsLine  = drop 2 $ words (strLines!!8)
            atoms      = sortBy (flip compareAtomIndex)
                       . map (readInDump (dumpHead []))
                       $ drop 9 strLines


readDumps :: FilePath -> IO [Dump]
readDumps fp = map (read . (delim++)) . tail . splitOn delim<$>readFile fp
    where delim = "ITEM: TIMESTEP"


class InDump a where
    readInDump :: Dump -> String -> a
    showInDump :: Dump -> Int -> a -> String

instance InDump Atom where
    readInDump d str = Atom (getReadCol' "id")
                            Nothing
                            (getReadCol' "type")
                            (Coord (getPos 'x')
                                   (getPos 'y')
                                   (getPos 'z'))
                     $ readColTail ["id","type","x","y","z","xs","ys","zs"]
        where
            readColTail :: [String] -> [(String,Double)]
            readColTail xs = map (mapSnd read)
                           $ filter (not . (`elem`xs) . fst) cols

            cols :: [(String,String)]
            cols = zip (atomsLine d) (words str)


            getCol :: String -> Maybe String
            getCol key = snd<$>find ((==key) . fst) cols

            getReadCol :: Read a => String -> Maybe a
            getReadCol key = read<$>getCol key

            getCol' :: String -> String
            getCol' = fromJust . getCol

            getReadCol' :: Read a => String -> a
            getReadCol' = read . getCol'


            getPos :: Char -> Double
            getPos dim = headDef (error ("No atom "++[dim]++"-data found"))
                       . catMaybes
                       $ [getReadCol [dim],scale<$>getReadCol (dim:"s")]
                where
                    scale x = let (dMin,dMax) = case dim of 'x' -> xLims d
                                                            'y' -> yLims d
                                                            'z' -> zLims d
                               in dMin + x*(dMax-dMin)

    showInDump d i a = unwords . filter (not . null)
                               $ [ padInt 4    $ i
                                 , show        $ kind    a
                                 , showElem    $ element a
                                 , showXYZ "x" $ x       a
                                 , showXYZ "y" $ y       a
                                 , showXYZ "z" $ z       a
                                 ]++map (uncurry showData) (atomData a)
        where
            showElem :: Maybe String -> String
            showElem (Just e) = e
            showElem Nothing  = ""

            showXYZ :: String -> Double -> String
            showXYZ xyzStr xyz = showDecimal 6
                               $ if xyzStr`elem`atomsLine d then xyz
                                                            else xyz'
                where
                    xyz' = (xyz-xyzMin)/(xyzMax-xyzMin)
                    (xyzMin,xyzMax) = case xyzStr of "x" -> xLims d
                                                     "y" -> yLims d
                                                     "z" -> zLims d

            padInt :: Int -> Int -> String
            padInt i x = replicate (i-length sx) ' '++sx where sx = show x

            showDecimal :: Int -> Double -> String
            showDecimal i = printf ("%."++show i++"f")

            showData :: String -> Double -> String
            showData _ = show
