{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
module Projection where

import           Control.Lens.Lens

import           Data.Composition
import           Data.List
import           Data.Tuple.Extra

import           MolDyn


range :: Double -> Double -> Double -> [Double]
range xMin xMax n = map ((+xMin) . (*(xMax-xMin)) . (/(n-1))) [0..n-1]

getRange :: (Double -> Double) -> String -> MolDyn [Double]
getRange f d = (\[min,max,n] -> range (f min) (f max) n)
                    <$>mapM getReadOption [d++"Min",d++"Max",d++"N"]

getXRange :: MolDyn [Double]
getXRange = getRange id "x"

getYRange :: MolDyn [Double]
getYRange = getRange id "y"

getAngRange :: MolDyn [Double]
getAngRange = getRange (*(pi/180)) "ang"

getCosAngRange :: MolDyn [Double]
getCosAngRange = map cos<$>getAngRange

getQRange :: MolDyn [Double]
getQRange = getRange id "q"

getQ'Range :: MolDyn [Double]
getQ'Range = getRange id "q'"

rangeBetweenSteps :: [Double] -> [Double]
rangeBetweenSteps r = zipWith ((/2).:(+)) r $ tail r

rangeTail :: [Double] -> [Double]
rangeTail (x:xs) = xs++[last xs-last (init xs)]


integrate :: [Double] -> [Double] -> [Double]
integrate xs axs = subtractHead $ integrateRec xs (sort axs) 0
    where
        subtractHead :: Num a => [a] -> [a]
        subtractHead (x:xs) = 0:map (x`subtract`) xs

        integrateRec :: [Double] -> [Double] -> Double -> [Double]
        integrateRec (x:xs)     []   i = i:integrateRec xs [] i
        integrateRec    []      _    i = []
        integrateRec (x:xs) (ax:axs) i
            | ax<x                     =   integrateRec (x:xs)     axs  (i+1)
            | otherwise                = i:integrateRec    xs  (ax:axs)  i

derivate :: [Double] -> [Double] -> [Double]
derivate xs ys = zipWith (-) ys (0:ys)


class Smoothable a where
    smoothOuter :: Int -> a -> a
    smoothInner :: Int -> Int -> a -> a

instance Smoothable [Double] where
    smoothOuter 0 xs = xs
    smoothOuter n xs = let avg x y z = 0.25*x+0.5*y+0.25*z
                        in smoothOuter (n-1)
                     $ head xs:zipWith3 avg xs (tail xs) (drop 2 xs)++[last xs]
    smoothInner l n xs
        | l < 0     = error "smoothing level too high"
        | otherwise = smoothOuter n xs

instance Smoothable [a] => Smoothable [[a]] where
    smoothOuter   n = transpose . map (smoothOuter n) . transpose
    smoothInner 0 n = smoothOuter n
    smoothInner l n = map $ smoothInner (l-1) n

getSmooth :: [Double] -> MolDyn [Double]
getSmooth = ((smoothOuter<$>getReadOption "nSmooth")??)

getInnerSmooth :: Smoothable a => Int -> a -> MolDyn a
getInnerSmooth l xs = (smoothInner l`flip`xs)<$>getReadOption "nSmooth"

getOuterSmooth :: Smoothable a => a -> MolDyn a
getOuterSmooth xs = (`smoothOuter`xs)<$>getReadOption "nSmooth"
