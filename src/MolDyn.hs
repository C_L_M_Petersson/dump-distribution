module MolDyn where

import qualified Configure           as Cfg
import           Control.Monad.Extra
import           Control.Monad.Loops
import           Control.Monad.State

import           MolDyn.Dump


type MolDyn = StateT Context IO

data Context = Context
               { dumpFocus :: Maybe Dump
               , cfgFocus  :: Maybe Cfg.Cfg
               , dumps     :: [Dump]
               , cfg       :: Cfg.Cfg
               }


evalMolDyn :: MolDyn a -> IO a
evalMolDyn s = do
    cfg <- Cfg.loadCfg
    ds  <- readDumps $ Cfg.getOption "dumpFile" cfg
    evalStateT s $ Context Nothing Nothing ds cfg


withDump :: MolDyn a -> Dump -> MolDyn a
withDump dc d = reFocusDump (Just d)>>dc>>=(reFocusDump Nothing>>) . return
    where
        reFocusDump :: Maybe Dump -> MolDyn ()
        reFocusDump md = modify $ \c -> c{ dumpFocus = md }

withCfg ::  MolDyn a -> String -> String -> MolDyn a
withCfg dc k v = reFocusCfg (Just (k,v))>>dc>>=(reFocusCfg Nothing>>) . return
    where
        reFocusCfg :: Maybe (String,String) -> MolDyn ()
        reFocusCfg Nothing      = modify
            $ \c -> c{ cfgFocus = Nothing }
        reFocusCfg (Just (k,v)) = getCfg>>= \cfg -> modify
            $ \c -> c{ cfgFocus = Just $ Cfg.insertCfg k v cfg }


liftIO :: IO a -> MolDyn a
liftIO = Control.Monad.State.liftIO


getDumps :: MolDyn [Dump]
getDumps = gets dumps

getDump :: MolDyn Dump
getDump = fromMaybeM (head<$>getDumps) (gets dumpFocus)

getsDump :: (Dump -> a) -> MolDyn a
getsDump = (<$>getDump)

modifyDumpMap :: (Dump -> Dump) -> MolDyn ()
modifyDumpMap f = getDumps>>=putDumps . map f

modifyDumpMapM :: (Dump -> MolDyn Dump) -> MolDyn ()
modifyDumpMapM f = getDumps>>=mapM f>>=putDumps

modifyDumps :: ([Dump] -> [Dump]) -> MolDyn ()
modifyDumps f = getDumps>>=putDumps . f

modifyDumpsM :: ([Dump] -> MolDyn [Dump]) -> MolDyn ()
modifyDumpsM f = getDumps>>=f>>=putDumps

putDumps :: [Dump] -> MolDyn ()
putDumps ds = modify $ \c -> c{ dumps = ds }

popDump :: MolDyn ()
popDump = getDumps>>=(\(_:ds) -> modify
                                    $ \c -> c{ dumpFocus = Nothing, dumps = ds })

mapDumpM_ :: MolDyn a -> MolDyn ()
mapDumpM_ = whileM_ (not . null<$>getDumps) . (>>popDump)


getCfg :: MolDyn Cfg.Cfg
getCfg = fromMaybeM (gets cfg) (gets cfgFocus)

getsCfg :: (Cfg.Cfg -> a) -> MolDyn a
getsCfg = (<$>getCfg)

getReadOption     :: Read a => String -> MolDyn a
getReadOption     = getsCfg . Cfg.getReadOption

getReadOptionSafe :: Read a => String -> MolDyn (Maybe a)
getReadOptionSafe = getsCfg . Cfg.getReadOptionSafe

getOption         ::           String -> MolDyn String
getOption         = getsCfg . Cfg.getOption

getOptionSafe     ::           String -> MolDyn (Maybe String)
getOptionSafe     = getsCfg . Cfg.getOptionSafe
