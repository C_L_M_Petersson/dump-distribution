module Geometry where

import           Data.Composition
import           Data.Function
import           Data.List
import           Data.Tuple.HT


class Positioned a where
    pos    :: a -> Coordinate
    to     :: a -> Coordinate -> a


data Coordinate = Coord Double Double Double

instance Eq Coordinate where
    Coord x y z==Coord x' y' z' = x==x' && y==y' && z==z'

instance Fractional Coordinate where
    fromRational r      = Coord r' r' r' where r' = fromRational r
    recip (Coord x y z) = Coord (1/x) (1/y) (1/z)

instance Num Coordinate where
    Coord x y z+Coord x' y' z' = Coord  (x+x')  (y+y')  (z+z')
    Coord x y z*Coord x' y' z' = Coord  (x*x')  (y*y')  (z*z')
    negate (Coord x y z)       = Coord    (-x)    (-y)    (-z)
    abs    (Coord x y z)       = Coord (abs x) (abs y) (abs z)
    fromInteger i              = Coord i' i' i' where i' = fromIntegral i

instance Positioned Coordinate where
    pos    = id
    to _ c = c

instance Show Coordinate where
    show (Coord x y z) = "("++show x++","++show y++","++show z++")"


data Direction = Dir Int Int Int

instance Eq Direction where
    d==d' = pos (reduceDir d)==pos (reduceDir d')

instance Positioned Direction where
    pos  (Dir   x y z) = Coord (fromIntegral x)
                               (fromIntegral y)
                               (fromIntegral z)
    to _ (Coord x y z) = Dir   (round x)
                               (round y)
                               (round z)

instance Read Direction where
    readsPrec _ "x" = [(Dir 1 0 0,"")]
    readsPrec _ "y" = [(Dir 0 1 0,"")]
    readsPrec _ "z" = [(Dir 0 0 1,"")]
    readsPrec i str = mapFst (uncurry3 Dir)`map`readsPrec i str

instance Show Direction where
    show = (\(Dir i j k) -> concatMap show [i,j,k]) . reduceDir

reduceDir :: Direction -> Direction
reduceDir (Dir i j k) = let d = i`gcd`gcd j k
                         in Dir (i`div`d) (j`div`d) (k`div`d)


type Range = (Double,Double)


x :: Positioned a => a -> Double
x = (\(Coord x _ _) -> x) . pos

y :: Positioned a => a -> Double
y = (\(Coord _ y _) -> y) . pos

z :: Positioned a => a -> Double
z = (\(Coord _ _ z) -> z) . pos


toX :: Positioned a => Double -> a -> a
toX x = fromTo $ \(Coord _ y z) -> Coord x y z

toY :: Positioned a => Double -> a -> a
toY y = fromTo $ \(Coord x _ z) -> Coord x y z

toZ :: Positioned a => Double -> a -> a
toZ z = fromTo $ \(Coord x y _) -> Coord x y z


fromTo :: Positioned a => (Coordinate -> Coordinate) -> a -> a
fromTo f p = p`to`f (pos p)

fromToX :: Positioned a => (Double -> Double) -> a -> a
fromToX f p = f (x p)`toX`p

fromToY :: Positioned a => (Double -> Double) -> a -> a
fromToY f p = f (y p)`toY`p

fromToZ :: Positioned a => (Double -> Double) -> a -> a
fromToZ f p = f (z p)`toZ`p


vecBetween :: Positioned a => a -> a -> Coordinate
vecBetween p0 p1 = Coord (x p1-x p0)
                         (y p1-y p0)
                         (z p1-z p0)


(<|>) :: (Positioned a,Positioned b) => a -> b -> Double
p<|>p' = x p*x p' + y p*y p' + z p*z p'

len2 :: Positioned a => a -> Double
len2 p = p<|>p

len :: Positioned a => a -> Double
len = sqrt . len2


inDir :: Positioned a => Direction -> a -> Double
inDir d p = pos d<|>pos p


(><) :: (Positioned a,Positioned b) => a -> b -> Coordinate
p><p' = Coord ( y p*z p' - z p*y p' )
              ( z p*x p' - x p*z p' )
              ( x p*y p' - y p*x p' )


scale :: Positioned a => Range -> Range -> Range -> a -> a
scale   (xMin,xMax) (yMin,yMax) (zMin,zMax) = fromTo scale'
    where scale' (Coord x y z) = Coord ( xMin + x*(xMax-xMin) )
                                       ( yMin + y*(yMax-yMin) )
                                       ( zMin + z*(zMax-zMin) )

unScale :: Positioned a => Range -> Range -> Range -> a -> a
unScale (xMin,xMax) (yMin,yMax) (zMin,zMax) = fromTo unScale'
    where unScale' (Coord x y z) = Coord ( (x-xMin)/(xMax-xMin) )
                                         ( (y-yMin)/(yMax-yMin) )
                                         ( (z-zMin)/(zMax-zMin) )


inRange :: Positioned a => Range -> a -> a -> Bool
inRange (dMin,dMax) p p' = within dMax p p'&&not (within dMin p p')

within :: Positioned a => Double -> a -> a -> Bool
within d p p'
    | abs(x p-x p')>d = False
    | abs(y p-y p')>d = False
    | abs(z p-z p')>d = False
    | otherwise       = dist2 p p'<=d**2

dist2 :: Positioned a => a -> a -> Double
dist2 = len2 .: vecBetween

dist  :: Positioned a => a -> a -> Double
dist  = sqrt .: dist2

sortByDist :: Positioned a => a -> [a] -> [a]
sortByDist a = sortBy $ distOrdering a

distOrdering :: Positioned a => a -> a -> a -> Ordering
distOrdering a a0 a1 = dist2 a a0`compare`dist2 a a1


angle :: Positioned a => a -> a -> a -> Double
angle = acos.:.cosAngle

cosAngle :: Positioned a => a -> a -> a -> Double
cosAngle p0 p1 p2 = v1<|>v2/sqrt(len2 v1*len2 v2)
    where
        v1 = p0`vecBetween`p1
        v2 = p0`vecBetween`p2

shift :: (Positioned a,Positioned b) => a -> b -> b
shift x = fromTo (+pos x)


averagePos :: Positioned a => [a] -> Coordinate
averagePos ps = let cs = map pos ps
                 in sum cs/genericLength cs
