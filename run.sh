DUMPPREFIX=inFiles/dump.
DUMPSUFFIX=.res
OUTFILEPREFIX=outFiles/
OUTFILESUFFIX=.res
OUTIMAGEPREFIX=outImages/


extractData()
{
    DUMPFILE="$DUMPPREFIX$1$DUMPSUFFIX"

    echo "Parsing dumpfile $DUMPFILE"

    echo "  - Calculating z-distribution of atoms"
    [ -e "$OUTFILEPREFIX"zDist."$1".res  ] ||               \
        ./run.x --task Dist  --dumpFile "$DUMPFILE"         \
            > "$OUTFILEPREFIX"zDist.$1$OUTFILESUFFIX
    [ -e "$OUTIMAGEPREFIX"zDist."$1".png ] ||               \
        gnuplot -c gp/plot.zDist.gp                         \
            "$OUTFILEPREFIX"zDist."$1".res                  \
            "$OUTIMAGEPREFIX"zDist."$1".png                 \
            "density, $2" $3 $4
    [ -e "$OUTIMAGEPREFIX"zProb."$1".png ] ||               \
        gnuplot -c gp/plot.zProb.gp                         \
            "$OUTFILEPREFIX"zDist."$1".res                  \
            "$OUTIMAGEPREFIX"zProb."$1".png                 \
            "$2" "$3" "$4"

    echo "  - Calculating peak position"
    [ -e "$OUTFILEPREFIX"zPeaks."$1".res  ] ||              \
        ./run.x --task Peaks --dumpFile "$DUMPFILE"         \
            > "$OUTFILEPREFIX"zPeaks."$1".res
    [ -e "$OUTIMAGEPREFIX"zPeaks."$1".png  ] ||             \
        gnuplot -c gp/plot.zPeaks.gp                        \
            "$OUTFILEPREFIX"zDist."$1".res                  \
            "$OUTFILEPREFIX"zPeaks."$1".res                 \
            "$OUTIMAGEPREFIX"zPeaks."$1".png                \
            "density, $2" "$3" "$4"

    echo "  - Calculating number of atoms per layer"
    [ -e "$OUTFILEPREFIX"byLayer."$1".res  ] ||             \
        ./run.x --task OxygenLayers --dumpFile "$DUMPFILE"  \
            > "$OUTFILEPREFIX"byLayer."$1".res
    [ -e "$OUTIMAGEPREFIX"byLayer."$1".png ] ||             \
        gnuplot -c gp/plot.byLayers.gp                      \
            "$OUTFILEPREFIX"byLayer."$1".res                \
            "$OUTIMAGEPREFIX"byLayer."$1".png               \
            "Atoms per layer, $2" "$3" "$4"

    echo "  - Calculating oxidation and bulk depth"
    [ -e "$OUTFILEPREFIX"zDepth."$1".res  ] ||              \
        ./run.x --task Depth --dumpFile "$DUMPFILE"         \
            > "$OUTFILEPREFIX"zDepth."$1".res
    [ -e "$OUTIMAGEPREFIX"zDepth."$1".png ] ||              \
        gnuplot -c gp/plot.zDepth.gp                        \
            "$OUTFILEPREFIX"zDepth."$1".res                 \
            "$OUTIMAGEPREFIX"zDepth."$1".png                \
            "Z-depth, $2" "$3" "$4"

    echo "  - Calculating radial distribution functions"
    [ -e "$OUTFILEPREFIX"radial."$1".res  ] ||              \
        ./run.x --task Radial --dumpFile "$DUMPFILE"        \
                --xMin 0 --xMax 5 --xN 1001 --nSmooth 50    \
            > "$OUTFILEPREFIX"radial."$1".res
    [ -e "$OUTIMAGEPREFIX"radial."$1".png ] ||              \
        gnuplot -c gp/plot.radial.gp                        \
            "$OUTFILEPREFIX"radial."$1".res                 \
            "$OUTIMAGEPREFIX"radial."$1".png                \
            "$2"

    echo "  - Calculating angular distribution functions"
    [ -e "$OUTFILEPREFIX"angular."$1".res  ] ||             \
        ./run.x --task Angular --dumpFile "$DUMPFILE"       \
            > "$OUTFILEPREFIX"angular."$1".res
    [ -e "$OUTIMAGEPREFIX"angular."$1".png ] ||             \
        gnuplot -c gp/plot.angular.gp                       \
            "$OUTFILEPREFIX"angular."$1".res                \
            "$OUTIMAGEPREFIX"angular."$1".png               \
            "$2"

    echo "  - Calculating 2D angular and radial distribution functions"
    [ -e "$OUTFILEPREFIX"angularRadial."$1".res  ] ||       \
        ./run.x --task AngularRadial --dumpFile "$DUMPFILE" \
                --xMin 0 --xMax 5 --xN 251 --nSmooth 0      \
            > "$OUTFILEPREFIX"angularRadial."$1".res
    [ -e "$OUTIMAGEPREFIX"angularRadial."$1".png ] ||       \
        gnuplot -c gp/plot.angularRadial.gp                 \
            "$OUTFILEPREFIX"angularRadial."$1".res          \
            "$OUTIMAGEPREFIX"angularRadial."$1".png         \
            "$2"

    echo "  - Calculating 2D distribution from direction"
    [ -e "$OUTFILEPREFIX"image2D."$1".res  ] ||              \
        ./run.x --task Image2DFromDir --dumpFile "$DUMPFILE" \
            > "$OUTFILEPREFIX"image2D."$1".res
    [ -e "$OUTIMAGEPREFIX"image2D.100.png ] ||               \
        (
            gnuplot -c gp/plot.image2D.gp                    \
                "$OUTFILEPREFIX"image2D."$1".res             \
                "$OUTIMAGEPREFIX"image2D."$1"                \
                "$2"
        ;
            gnuplot -c gp/plot.image2D.gp                    \
                "$OUTFILEPREFIX"image2D."$1".res             \
                "$OUTIMAGEPREFIX"image2D..comparison."$1"    \
                "$2"
        )
}

extractData   noO.xScale=1.0.yScale=1.0 "without oxygen" 1.0 1000
extractData withO.xScale=1.0.yScale=1.0 "   with oxygen" 1.0 1000
